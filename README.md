## Blog/Comment API using Laravel Sail, with a testing system and Swagger documentation

API:

```
GET /blogs
POST /blogs
GET /blogs/{blog}
PUT /blogs/{blog}
DELETE /blogs/{blog}
GET /blogs/{blog}/comments
```

```
GET /comments
POST /comments
GET /comments/{comment}
PUT /comments/{comment}
DELETE /comments/{comment}
```