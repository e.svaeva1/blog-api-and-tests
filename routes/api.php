<?php

use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\CommentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* View .../api/documentation for Swagger Documentation */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'blogs'], function () {
    Route::get('/', [BlogController::class, 'index'])->name('blogsIndex');
    Route::post('/', [BlogController::class, 'store'])->name('blogCreate');
    Route::get('/{blog}', [BlogController::class, 'show'])->name('blogShow');
    Route::put('/{blog}', [BlogController::class, 'update'])->name('blogUpdate');
    Route::delete('/{blog}', [BlogController::class, 'destroy'])->name('blogDelete');
    Route::get('/{blog}/comments', [BlogController::class, 'comments'])->name('blogComments');
});

Route::group(['prefix' => 'comments'], function () {
    Route::get('/', [CommentController::class, 'index'])->name('commentsIndex');
    Route::post('/', [CommentController::class, 'store'])->name('commentCreate');
    Route::get('/{comment}', [CommentController::class, 'show'])->name('commentShow');
    Route::put('/{comment}', [CommentController::class, 'update'])->name('commentUpdate');
    Route::delete('/{comment}', [CommentController::class, 'destroy'])->name('commentDelete');
});