<?php

namespace App\Exceptions\Api;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Translation\Translator;
use JsonSerializable;

/**
 * APIException
 *
 * @package App\Exceptions\Api
 */
class ApiException extends Exception implements JsonSerializable
{
    /** Ошибка по умолчанию  */
    const DEFAULT = 'errors.default';
    /** Ресурс не найден */
    const MODEL_NOT_FOUND = 'errors.model_not_found';
    /** Ошибка валидации */
    const VALIDATION_EXCEPTION = 'errors.validation_error';
    /** Ошибка авторизации */
    const UNAUTHENTICATED_EXCEPTION = 'errors.unauthenticated';
    /** Страница не найдена */
    const PAGE_NOT_FOUND_EXCEPTION = 'errors.page_not_found';
    /** Метод не поддерживается */
    const METHOD_NOT_ALLOWED = 'errors.method_not_allowed';
    /** Почта уже подтверждена */
    const EMAIL_ALREADY_VERIFIED = 'errors.email_already_verified';
    /** Доступ запрещен */
    const ACCESS_DENIED = 'errors.access_denied';

    /** Соответствие HTTP-кодов и репортов типам ошибок */
    const SETTINGS = [
        self::DEFAULT                           => ['statusCode' => 500, 'report' => true],
        self::MODEL_NOT_FOUND                   => ['statusCode' => 404, 'report' => false],
        self::VALIDATION_EXCEPTION              => ['statusCode' => 422, 'report' => false],
        self::UNAUTHENTICATED_EXCEPTION         => ['statusCode' => 401, 'report' => false],
        self::PAGE_NOT_FOUND_EXCEPTION          => ['statusCode' => 404, 'report' => false],
        self::METHOD_NOT_ALLOWED                => ['statusCode' => 405, 'report' => false],
        self::EMAIL_ALREADY_VERIFIED            => ['statusCode' => 403, 'report' => false],      
        self::ACCESS_DENIED                     => ['statusCode' => 403, 'report' => false],
    ];

    /**
     * HTTP статус-код
     *
     * @var int
     */
    private int $httpCode;

    /**
     * Массив ошибок
     *
     * @var array
     */
    public array $errors;

    /**
     * Внутренний код исключения
     *
     * @var string
     */
    public $code;

    /**
     * Определяет необходимость логирования
     *
     * @var bool|mixed
     */
    private bool $needReport;

    /**
     * Стектрейс оригинального исключения
     *
     * @var array
     */
    private array $trace;

    /**
     * ApiException constructor.
     *
     * @param string $code
     * @param string|null $message
     * @param array $errors
     * @param array $trace
     * @param int|null $httpCode
     * @param bool|null $needReport
     */
    public function __construct(
        string $code = self::DEFAULT,
        string $message = null,
        array $errors = [],
        array $trace = [],
        int $httpCode = null,
        bool $needReport = null
    ) {
        $this->errors = $errors;
        $this->httpCode = $httpCode ?? static::SETTINGS[$code]['statusCode'];
        $this->code = $code;
        $this->needReport = $needReport ?? static::SETTINGS[$code]['report'];
        $this->trace = $trace;
        parent::__construct($message ?? __($code));
    }

    /**
     * HTTP статус код ошибки
     *
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * Переводим сообщение и частный случай для прода
     *
     * @param string $message
     *
     * @return array|string|Translator|Application|null
     */
    private static function translateMessage(string $message): array|string|Translator|Application|null
    {
        if (!config('app.debug')) {
            $message = static::DEFAULT;
        }
        return __($message);
    }

    /**
     * Определяет, необходимо ли логировать исключение
     *
     * @return bool
     */
    public function report(): bool
    {
        return !$this->needReport;
    }

    /**
     * Определяет формат для json_encode()
     * @return array[]
     */
    public function jsonSerialize(): array
    {
        $res = [
            'code' => $this->code,
            'message' => static::translateMessage($this->message),
            'advanced' => $this->errors,
        ];
        if (config('app.debug') && $this->getHttpCode() >= 500 && $this->trace) {
            $res['stacktrace'] = $this->trace;
        }
        return [
            'error' => $res,
        ];
    }

    /**
     * Добавляет информацию в лог
     *
     * @return array
     */
    public function context(): array
    {
        return ['http_status' => $this->httpCode, 'errors' => $this->errors];
    }
}