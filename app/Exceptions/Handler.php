<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\Api\ApiException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ApiException $e) {
            return response()->json($e, $e->getHttpCode());
        });
    }

    /**
     * @param Throwable $e
     * @return ApiException
     * @throws Throwable
     */
    protected function prepareException(Throwable $e): ApiException
    {
        switch (true) {
            case $e instanceof ApiException:
                break;
            /** Ошибка валидации */
            case $e instanceof ValidationException:
                $errors = $e->validator->errors()->getMessages();
                $e = new ApiException(ApiException::VALIDATION_EXCEPTION, null, $errors);
                break;
            /** Ресурс не найден */
            case $e instanceof ModelNotFoundException:
                $model = static::getModelName($e);
                $e = new ApiException(ApiException::MODEL_NOT_FOUND, __("not_founds.$model"));
                break;
            /** Ошибка авторизации */
            case $e instanceof UnauthorizedException:
            case $e instanceof AuthenticationException:
                $e = new ApiException(ApiException::UNAUTHENTICATED_EXCEPTION);
                break;
            /** Неправильная ссылка */
            case $e instanceof NotFoundHttpException:
                $e = new ApiException(ApiException::PAGE_NOT_FOUND_EXCEPTION);
                break;
            case $e instanceof MethodNotAllowedHttpException:
                $e = new ApiException(ApiException::METHOD_NOT_ALLOWED);
                break;
            /** Доступ запрещен */
            case $e instanceof AccessDeniedHttpException:
            case $e instanceof AuthorizationException:
                $e = new ApiException(ApiException::ACCESS_DENIED);
                break;    
            /** Для остальных */
            default:
                $e = new ApiException(ApiException::DEFAULT, $e->getMessage(), [], $e->getTrace());
        }
        return $e;
    }

    /**
     * @param ModelNotFoundException $e
     * @return mixed
     */
    private static function getModelName(ModelNotFoundException $e): string
    {
        $model = (string)($e->getModel());
        return last(explode('\\', $model));
    }
}
