<?php

namespace App\Repositories;

use App\Models\Blog;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use Illuminate\Support\Collection;

class BlogRepository implements BlogRepositoryInterface
{
    /**
     * index
     *
     * @return Collection
     */
    public function index(): Collection
    {
        $blogs = Blog::all();
        return $blogs;
    }

    /**
     * create
     *
     * @param  array $data
     * @return Blog
     */
    public function create(array $data): Blog
    {
        $blog = Blog::create([
            'name' => $data['name'],
            'content' => $data['content'],
        ]);
        return $blog;
    }

    /**
     * update
     *
     * @param  Blog $blog
     * @param  array $data
     * @return bool
     */
    public function update(Blog $blog, array $data): bool
    {
        $result = $blog->update([
            'name' => $data['name'] ?? $blog->name,
            'content' => $data['content'] ?? $blog->content,
        ]);
        return $result;
    }
    
    /**
     * delete
     *
     * @param  Blog $blog
     * @return bool
     */
    public function delete(Blog $blog): bool
    {
        return $blog->delete();
    }

    /**
     * comments
     *
     * @param  Blog $blog
     * @return Collection
     */
    public function comments(Blog $blog): Collection
    {
        return $blog->comments;
    }
}