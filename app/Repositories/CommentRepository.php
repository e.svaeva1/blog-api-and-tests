<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Illuminate\Support\Collection;

class CommentRepository implements CommentRepositoryInterface
{
    /**
     * index
     *
     * @return Collection
     */
    public function index(): Collection
    {
        $comment = Comment::all();
        return $comment;
    }

    /**
     * create
     *
     * @param  array $data
     * @return Comment
     */
    public function create(array $data): Comment
    {
        $comment = Comment::create([
            'text' => $data['text'],
            'blog_id' => $data['blog_id'],
        ]);
        return $comment;
    }

    /**
     * update
     *
     * @param  Comment $comment
     * @param  array $data
     * @return bool
     */
    public function update(Comment $comment, array $data): bool
    {
        $result = $comment->update([
            'text' => $data['text'] ?? $comment->text,
        ]);
        return $result;
    }
    
    /**
     * delete
     *
     * @param  Comment $comment
     * @return bool
     */
    public function delete(Comment $comment): bool
    {
        return $comment->delete();
    }
}