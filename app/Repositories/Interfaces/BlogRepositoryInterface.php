<?php

namespace App\Repositories\Interfaces;

use App\Models\Blog;
use Illuminate\Support\Collection;

interface BlogRepositoryInterface
{
    /**
     * Get all blogs.
     *
     * @return Collection
     */
    public function index(): Collection;

    /**
     * Create blog.
     *
     * @param  array $data
     * @return Blog
     */
    public function create(array $data): Blog;

    /**
     * Update blog.
     *
     * @param  Blog $blog
     * @param  array $data
     * @return bool
     */
    public function update(Blog $blog, array $data): bool;
    
    /**
     * Delete blog.
     *
     * @param  Blog $blog
     * @return bool
     */
    public function delete(Blog $blog): bool;

    /**
     * Get blog comments.
     *
     * @param  Blog $blog
     * @return Collection
     */
    public function comments(Blog $blog): Collection;
}