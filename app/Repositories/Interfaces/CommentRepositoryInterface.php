<?php

namespace App\Repositories\Interfaces;

use App\Models\Comment;
use Illuminate\Support\Collection;

interface CommentRepositoryInterface
{
    /**
     * Get all existing comments.
     *
     * @return Collection
     */
    public function index(): Collection;

    /**
     * Create comment.
     *
     * @param  array $data
     * @return Comment
     */
    public function create(array $data): Comment;

    /**
     * Update comment.
     *
     * @param  Comment $comment
     * @param  array $data
     * @return bool
     */
    public function update(Comment $comment, array $data): bool;
    
    /**
     * Delete comment.
     *
     * @param  Comment $comment
     * @return bool
     */
    public function delete(Comment $comment): bool;
}