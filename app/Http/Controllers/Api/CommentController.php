<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\CreateRequest;
use App\Http\Requests\Comment\UpdateRequest;
use App\Http\Resources\Comment\DeletedResource;
use App\Http\Resources\Comment\CommentResource;
use App\Http\Resources\Comment\CommentShortResource;
use App\Http\Resources\Comment\UpdatedResource;
use App\Models\Comment;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentController extends Controller
{
    public function __construct(
        protected CommentRepositoryInterface $commentRepository) { }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $result = $this->commentRepository->index();
        return CommentShortResource::collection($result);
    }

    /**
     * store
     *
     * @param  CreateRequest $request
     * @return void
     */
    public function store(CreateRequest $request)
    {
        $result = $this->commentRepository->create($request->validated());
        return new CommentResource($result);
    }

    /**
     * show
     *
     * @param Blog $blog
     * @param Request $request
     * @return void
     */
    public function show(Comment $comment, Request $request)
    {
        return new CommentResource($comment);
    }

    /**
     * update
     *
     * @param Comment $comment
     * @param UpdateRequest $request
     * @return void
     */
    public function update(Comment $comment, UpdateRequest $request)
    {
        $result = $this->commentRepository->update($comment, $request->validated());
        return new UpdatedResource($result);
    }

    /**
     * destroy
     *
     * @param Blog $blog
     * @param Request $request
     * @return void
     */
    public function destroy(Comment $comment, Request $request)
    {
        $result = $this->commentRepository->delete($comment);
        return new DeletedResource($result);
    }
}