<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\CreateRequest;
use App\Http\Requests\Blog\UpdateRequest;
use App\Http\Resources\Blog\DeletedResource;
use App\Http\Resources\Blog\BlogResource;
use App\Http\Resources\Blog\UpdatedResource;
use App\Http\Resources\Comment\CommentShortResource;
use App\Models\Blog;
use App\Repositories\Interfaces\BlogRepositoryInterface;

class BlogController extends Controller
{
    public function __construct(
        protected BlogRepositoryInterface $blogRepository) { }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $result = $this->blogRepository->index();
        return BlogResource::collection($result);
    }

    /**
     * store
     *
     * @param  CreateRequest $request
     * @return void
     */
    public function store(CreateRequest $request)
    {
        $result = $this->blogRepository->create($request->validated());
        return new BlogResource($result);
    }

    /**
     * show
     *
     * @param Blog $blog
     * @param Request $request
     * @return void
     */
    public function show(Blog $blog, Request $request)
    {
        return new BlogResource($blog);
    }

    /**
     * update
     *
     * @param Blog $blog
     * @param UpdateRequest $request
     * @return void
     */
    public function update(Blog $blog, UpdateRequest $request)
    {
        $result = $this->blogRepository->update($blog, $request->validated());
        return new UpdatedResource($result);
    }

    /**
     * destroy
     *
     * @param Blog $blog
     * @param Request $request
     * @return void
     */
    public function destroy(Blog $blog, Request $request)
    {
        $result = $this->blogRepository->delete($blog);
        return new DeletedResource($result);
    }

    /**
     * comments
     *
     * @param Blog $blog
     * @param Request $request
     * @return void
     */
    public function comments(Blog $blog, Request $request)
    {
        $result = $this->blogRepository->comments($blog);
        return CommentShortResource::collection($result);
    }
}
