<?php

namespace Tests\Feature;

use App\Models\Blog;

class BlogTest extends Base
{
    public const INDEX_AMT = 10;
    public const COMMENTS_AMT = 20;

    protected array $blogStructure = [
        'id',
        'name',
        'content',
        'comments' => [
            '*' => [
                'id',
                'text',
                'blog_id',
            ]
        ],
    ];

    /**
     * Test blog index() for ok status and expected amount
     *
     * @return void
     */
    public function testIndex() {
        $response = $this->sendRequest('blogsIndex')
            ->assertOk();
        $this->testIndexStructure($response, $this->blogStructure);
        $this->assertEquals(self::INDEX_AMT, count($response->json('data')));
    }

    /**
     * Test blog create() for status, JSON response and existing
     *
     * @return void
     */
    public function testCreate()
    {
        $createData = [
            'name' => 'Название нового блога',
            'content' => 'Содержание блога',
        ];

        $this->sendRequest('blogCreate', [], $createData)
            ->assertCreated()
            ->assertJsonStructure(["data" => $this->blogStructure])
            ->assertJsonFragment($createData);
        $this->assertDatabaseHas(Blog::class, $createData);
    }
    
    /**
     * Test blog show() for status, JSON response and valid comments
     *
     * @return void
     */
    public function testShow()
    {
        $blog = Blog::with('comments')->first();
        unset($this->blogStructure['comments']);

        $response = $this->sendRequest('blogShow', ['blog' => $blog->id])
            ->assertOk()
            ->assertJsonStructure(['data' => $this->blogStructure])
            ->assertJsonFragment($blog->only($this->blogStructure))
            ->assertJsonFragment(['comments' => $blog->comments->toArray()]);
        $this->assertCount(self::COMMENTS_AMT, $response->json('data.comments'));
    }

    /**
     * Test blog update() for status, JSON response and correct values
     *
     * @return void
     */
    public function testUpdate()
    {
        $blog = Blog::query()->first();
        $updateData = [
            'name' => 'Название обновленного блога',
            'content' => 'Содержание обновленного блога',
        ];

        $this->sendRequest('blogUpdate', ['blog' => $blog->id], $updateData)
            ->assertOk()
            ->assertJsonFragment(['updated' => true]);
        $this->assertModelExists($blog);
        $blog->refresh();
        foreach ($updateData as $key => $value) {
            $this->assertEquals($value, $blog->$key);
        }
    }

    /**
     * Test blog destroy() for status, missing blog and comments
     *
     * @return void
     */
    public function testDestroy()
    {
        $blog = Blog::with('comments')->first();

        $this->sendRequest('blogDelete', ['blog' => $blog->id])
            ->assertOk()
            ->assertJsonFragment(['deleted' => true]);
        $this->assertModelMissing($blog);
        foreach ($blog->comments as $comment) {
            $this->assertModelMissing($comment);
        }
    }

    /**
     * Test getting blog comments for status, JSON and amount
     *
     * @return void
     */
    public function testGetComments()
    {
        $blog = Blog::query()->first();
        $response = $this->sendRequest('blogComments', ['blog' => $blog->id])
            ->assertOk()
            ->assertJsonStructure(['data' => $this->blogStructure['comments']]);
        $this->assertCount(self::COMMENTS_AMT, $response->json('data'));
    }
}
