<?php

namespace Tests\Feature;

use Database\Seeders\TestSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class Base extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Auth response
     *
     * @var array|string[][]
     */
    protected array $authStruct = [
        "data" => [
            "access_token",
            "token_type",
            "expires_in",
            "message"
        ]
    ];

    /**
     * Message response
     *
     * @var array|string[][]
     */
    protected array $successStruct = [
        "data" => ["message"]
    ];

    /**
     * Error response
     *
     * @var array|array[]
     */
    protected array $errorStruct = [
        "error" => [
            "code",
            "message",
            "advanced" => []
        ]
    ];

    protected $defaultHeaders = ['Accept' => "application/json"];

    /**
     * Send request
     * 
     * @param  string $routeName
     * @param  array $routeParams
     * @param  array $data
     * @param  array $headers
     * @param  string|null $method
     * @return TestResponse
     */
    protected function sendRequest(
        string $routeName,
        array $routeParams = [],
        array $data = [],
        array $headers = [],
        string $method = null
        ): TestResponse
    {
        if (!$method) {
            $router = app('Illuminate\Routing\Router');
            if (!is_null($route = $router->getRoutes()->getByName($routeName))) {
                $method = $route->methods()[0];
            }
        }
        $headers = array_merge($this->defaultHeaders, $headers);
        return $this->json($method, $this->getUrl($routeName, $routeParams), $data, $headers);
    }

    /**
     * Get url from route name
     * 
     * @param  string $route
     * @param  array $params
     * @return string
     */
    protected function getUrl(string $route, array $params = []): string
    {
        return route($route, $params, false);
    }

    /**
     * Test index
     * 
     * @param TestResponse $response
     * @param array $struct
     * @return void
     */
    protected function testIndexStructure(TestResponse $response, array $struct = []): void
    {
        $response
            ->assertOk()
            ->assertJsonStructure(["data" => ['*' => $struct]]);
    }

    /**
     * Preparing to test
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(TestSeeder::class);
    }
}
