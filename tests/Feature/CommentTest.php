<?php

namespace Tests\Feature;

use App\Models\Blog;
use App\Models\Comment;

class CommentTest extends Base
{
    public const INDEX_AMT = BlogTest::INDEX_AMT * BlogTest::COMMENTS_AMT;

    protected array $commentShortStructure = [
        'id',
        'text',
        'blog_id',
    ];

    protected array $commentStructure = [
        'id',
        'text',
        'blog' => [
            'id',
            'name',
            'content',
        ],
    ];

    /**
     * Test comment index() for ok status and expected amount
     *
     * @return void
     */
    public function testIndex() {
        $response = $this->sendRequest('commentsIndex')
            ->assertOk();
        $this->testIndexStructure($response, $this->commentShortStructure);
        $this->assertEquals(self::INDEX_AMT, count($response->json('data')));
    }

    /**
     * Test comment creation for status, JSON response and existing
     *
     * @return void
     */
    public function testCreate()
    {
        $blog = Blog::query()->first();
        $createData = [
            'blog_id' => $blog->id,
            'text' => 'Новый комментарий',
        ];

        $this->sendRequest('commentCreate', [], $createData)
            ->assertCreated()
            ->assertJson(['data' => [
                'text' => $createData['text'],
                'blog' => $blog->only($this->commentStructure['blog'])
            ]]);
        $this->assertDatabaseHas(Comment::class, $createData);
        
        $existsInComments = false;
        foreach ($blog->comments as $blogComment) {
            $existsInComments = $existsInComments || ($blogComment->text == $createData['text']);
        }
        $this->assertTrue($existsInComments);
    }

    /**
     * Test comment show for status and JSON response
     *
     * @return void
     */
    public function testShow()
    {
        $comment = Comment::with('blog')->first();
        $response = $this->sendRequest('commentShow', ['comment' => $comment->id])
            ->assertOk()
            ->assertJsonStructure(['data' => $this->commentStructure]);
        $this->assertEquals($response->json('data.text'), $comment->text);
        $this->assertEquals($response->json('data.blog.id'), $comment->blog->id);

    }

    /**
     * Test comment update for status, JSON response and existing
     *
     * @return void
     */
    public function testUpdate()
    {
        $comment = Comment::with('blog')->first();
        $updateData = [
            'text' => 'Обновленный комментарий',
        ];
        $this->sendRequest('commentUpdate', ['comment' => $comment->id], $updateData)
            ->assertOk()
            ->assertJsonFragment(['updated' => true]);
        $comment->refresh();
        $this->assertEquals($updateData['text'], $comment->text);
    }

    /**
     * Test comment deleting for status, JSON response and not existing
     *
     * @return void
     */
    public function testDestroy()
    {
        $comment = Comment::with('blog')->first();
        $this->sendRequest('commentDelete', ['comment' => $comment->id])
            ->assertOk()
            ->assertJsonFragment(['deleted' => true]);
        $this->assertModelMissing($comment);
    }
}
