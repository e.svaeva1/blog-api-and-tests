<?php

namespace Database\Seeders;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Faker\Factory;
use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Comment::exists()) {
            return;
        }
        $blogs = Blog::all();
        $faker = Factory::create();
        foreach ($blogs as $blog) {
            for ($i = 0; $i < 20; $i++) {
                Comment::create([
                    'text' => $faker->realText(450),
                    'blog_id' => $blog->id,
                ]);
            }
        }
    }
}
