<?php

namespace Database\Seeders;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Blog::exists()) {
            for ($i = 0; $i < 10; $i++) {
                Blog::create([
                    'name' => 'Блог #' . $i,
                    'content' => 'Содержание блога ' . $i,
                ]);
            }
        }
    }
}
